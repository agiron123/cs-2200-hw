main:	
		la	$sp, stack					! load address of stack into $sp
		lw  $sp, 0($sp) 				!load the value of stack into stack pointer
		la	$at, pow					! load address of pow into $at
		addi $a0, $zero, 2				! $a0 = 0, the base for pow
		addi $a1, $zero, 3				! $a1 = 3, the power for pow
		addi $t2, $sp, -1				!move the frame pointer to one above the stack pointer

		jalr $at, $ra					! jump to pow, set $ra to return addr
		
		lw $t0, 0($sp)					!load the value stored at stack pointer into t0
		la $t1, answer					!load address of answer into $t1
		sw $t0, 0($t1)					!store t0 which holds final result at address of answer label

		stack:	.word 0xF000			! the stack begins here (for example, that is)
		answer: .word 0					!where the answer is going to be stored
		halt							! halt and end program execution
		
pow:
		addi $sp, $sp, -2   		    !Make room on the stack for parameters
		sw $a1, 1($sp)		 			!store the first parameter
		sw $a0, 0($sp)		 			!store the second parameter
		
		addi $sp, $sp, -3				!make room on stack for arguments, return value, return address and old frame pointer
		sw $t2, 0($sp) 					!store the old frame pointer
		sw $ra, 1($sp)					!store the old return address

		addi $t2, $sp, -1				!move the frame pointer to one above the stack pointer
		lw $a1, 5($t2)					!load the exponent parameter into $a1
		
		beq $zero, $a1, base			!go to base case if exponent is 0
		beq $zero, $a0, zerobasecase	!we are raising zero to a power other than 0, so return 0
		beq $zero, $zero, regular       !else go do the multiplication
										!return pow(base, exponent-1) * base

zerobasecase:
		beq $zero, $zero, intermediate	
base:   
		addi $s2, $zero, 1				!put a 1 into $s2
		beq	$a1, $zero, intermediate	!break to an intermediate part of the code

regular: 
		addi $a1, $a1, -1				!subtract 1 from exponent and store in a temporary register
		la	$at, pow					! load address of pow into $at
		jalr $at, $ra					! jump to pow, set $ra to return addr
		lw $t0, 0($sp)					!load the return value and add to itself base-1 times
		lw $s2, 0($sp)					!load value into another register								
		add $t1, $a0, $zero 			!copy $a0 to $t1
		
		beq $zero, $zero, mult			!skip the intermediate case
		
intermediate:
		sw $s2, 3($t2)					!store the return value
		beq $zero, $zero, baseend

mult:	
		addi $t1, $t1, -1				!subtract 1 from base
		add $t0, $t0, $s2        		!add base to accumulator register 
		beq $zero, $t1, multend   		!check if the loop is over
		addi $t1, $t1, -1	     	    !decrement t1 for the loop
		beq $zero, $zero, mult			!go back to the top of the loop
multend:
		addi $sp, $sp, 3 				!pop the stack off
		sw $t0, 3($t2)					!store the return value
baseend:
		lw $t2, 0($sp)					!load the old frame pointer
		lw $ra, 1($sp)					!load the previous return address
		addi $sp, $sp, 2				!make the stack pointer point to the return value
		jalr $ra, $s2					!go to the return address
