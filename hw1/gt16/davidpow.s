; David Greenhalgh
; CS 2200
; HW1, problem 2B   

    addi $a0, $zero, 2
    addi $a1, $zero, 4
    
    la $sp, stack
    la $at, pow
    jalr $at, $ra       ; jump to pow subroutine
    la $t0, answer
    sw $v0, 0($t0)
    halt

; pow subroutine grows the stack
pow sw $v0, 0($sp)
    sw $ra, 1($sp)
    addi $sp, $sp, 2
    beq $a1, $zero, first
    addi $a1, $a1, -1
    jalr $at, $ra
    beq $a1, $zero, reg

; regular recursive case
reg add $t0, $v0, $zero
    beq $zero, $zero, multi
done sw $v0, -2($sp)
    beq $zero, $zero, clean

; first recursive case
first add $v0, $a0, $a0
    sw $v0, -2($sp)
    ;addi $sp, $sp, -2
    ;beq $zero, $zero, reg
    beq $zero, $zero, clean

; clean up
clean addi $sp, $sp, -2
    lw $t1, 1($sp)
    jalr $t1, $ra

; multiplication subroutine
multi add $v0, $t0, $v0
    addi $a0, $a0, -1
    beq $a0, $zero, done
    beq $zero, $zero, multi

answer  .word 0
stack   .byte 0
