!************************************************************************!
!									 !
! general calling convention:						 !
!									 !
! (1) Register usage is as implied in the assembler names		 !
!									 !
! (2) Stack convention							 !
!									 !
!	  The stack grows towards higher addresses.  The stack pointer	 !
!	  ($sp) points to the next available (empty) location.		 !
!									 !
! (3) Mechanics								 !
!									 !
!	  (3a) Caller at call time:					 !
!	       o  Write any caller-saved stuff not saved at entry to	 !
!		  space on the stack that was reserved at entry time.	 !
!	       o  Do a JALR leaving the return address in $ra		 !
!									 !
!	  (3b) Callee at entry time:					 !
!	       o  Reserve all stack space that the subroutine will need	 !
!		  by adding that number of words to the stack pointer,	 !
!		  $sp.							 !
!	       o  Write any callee-saved stuff ($ra) to reserved space	 !
!		  on the stack.						 !
!	       o  Write any caller-saved stuff if it makes sense to	 !
!		  do so now.						 !
!									 !
!	  (3c) Callee at exit time:					 !
!	       o  Read back any callee-saved stuff from the stack ($ra)	 !
!	       o  Deallocate stack space by subtract the number of words !
!		  used from the stack pointer, $sp			 !
!	       o  return by executing $jalr $ra, $zero.			 !
!									 !
!	  (3d) Caller after return:					 !
!	       o  Read back any caller-saved stuff needed.		 !
!									 !
!************************************************************************!

!vector table
 vector0: .fill 0x00000000 !0
 .fill 0x00000000 !1
 .fill 0x00000000 !2
 .fill 0x00000000
 .fill 0x00000000 !4
 .fill 0x00000000
 .fill 0x00000000
 .fill 0x00000000
 .fill 0x00000000 !8
 .fill 0x00000000
 .fill 0x00000000
 .fill 0x00000000
 .fill 0x00000000
 .fill 0x00000000
 .fill 0x00000000
 .fill 0x00000000 !15
!end vector table


main:
	addi $sp, $zero, initsp ! initialize the stack pointer
	lw $sp, 0($sp)
	! Install timer interrupt handler into the vector table
	!Load the starting address of the handler into the interrupt vector table at address 0x000001.
    di !not sure if were supposed to do this here or not
    la $a0, ti_inthandler !load interrupt handlers address into a0
    sw $a0, 0x1($zero)   !store the interrupt handler's address to address 0x000001 on the IVT
	ei 			!Don't forget to enable interrupts...
	addi $a0, $zero, 4	!load base for pow
	addi $a1, $zero, 10	!load power for pow
	addi $at, $zero, POW			!load address of pow
	jalr $at, $ra		!run pow
		
	halt	
		

POW: 
  addi $sp, $sp, 2   ! push 2 slots onto the stack
  sw $ra, -1($sp)   ! save RA to stack
  sw $a0, -2($sp)   ! save arg 0 to stack
  beq $zero, $a1, RET1 ! if the power is 0 return 1
  beq $zero, $a0, RET0 ! if the base is 0 return 0
  addi $a1, $a1, -1  ! decrement the power
  la $at, POW	! load the address of POW
  jalr $at, $ra   ! recursively call POW
  add $a1, $v0, $zero  ! store return value in arg 1
  lw $a0, -2($sp)   ! load the base into arg 0
  la $at, MULT		! load the address of MULT
  jalr $at, $ra   ! multiply arg 0 (base) and arg 1 (running product)
  lw $ra, -1($sp)   ! load RA from the stack
  addi $sp, $sp, -2  ! pop the RA and arg 0 off the stack
  jalr $ra, $zero   ! return
RET1: addi $v0, $zero, 1  ! return a value of 1
  addi $sp, $sp, -2
  jalr $ra, $zero
RET0: add $v0, $zero, $zero ! return a value of 0
  addi $sp, $sp, -2
  jalr $ra, $zero		
	
MULT: add $v0, $zero, $zero ! zero out return value
AGAIN: add $v0,$v0, $a0  ! multiply loop
  addi $a1, $a1, -1
  beq $a1, $zero, DONE ! finished multiplying
  beq $zero, $zero, AGAIN ! loop again
DONE: jalr $ra, $zero	
		
!Start coding here		
ti_inthandler:

	!first save the current value of k0 and the state of the interrupted program
	di
	addi $sp, $sp, 7   ! push 7 slots onto the stack
	sw $k0, 0($sp)   !store k0 onto the stack
	ei !enable interrupts
	sw $sp, -1($sp)
	sw $a0, -2($sp)
	sw $a1, -3($sp)
	sw $at, -4($sp)
	sw $ra, -5($sp)
	sw $v0, -6($sp)
	
	!increment the clock variable in memory
		
		!load seconds to a0
		addi $a4, $zero, seconds
		lw $a0, 0($a4)
		lw $a0, 0($a0)

		!load minutes to a1
		addi $a4, $zero, minutes
		lw $a1, 0($a4)
		lw $a1, 0($a1)
		
		!load hours to a2
		addi $a4, $zero, hours
		lw $a2, 0($a4)
		lw $a2, 0($a2)
	
		!subtract 59 from seconds, break if equal to zero
		addi $a4, $a0, -59
		beq $zero, $a4, secondsReset
		addi $a0, $a0, 1 !increment seconds if its not equal to 59
		beq $zero, $zero, storeTime !store time in memory and move on

		secondsReset:
		addi $a0, $zero, 0 !set a0 to 0
		!check if minutes is 60, increment it if not
		
		addi $a4, $a1, -59
		beq $zero, $a4, minReset
		addi $a1, $a1, 1 !increment minutes if its not equal to 60
		beq $zero, $zero, storeTime !store time in memory and move on

		minReset:
		addi $a1, $zero, 0 !set a1 to 0
		addi $a2, $a2, 1 !increment hours since there were 60 minutes that passed
	
	storeTime:
		!store seconds, minutes and hours into memory
		
		addi $a4, $zero, seconds
		lw $a4, 0($a4)
		sw $a0, 0($a4)

		addi $a4, $zero, minutes
		lw $a4, 0($a4)
		sw $a1, 0($a4)
		
		addi $a4, $zero, hours
		lw $a4, 0($a4)
		sw $a2, 0($a4)	
	
	!restore the state of the original program and return to where it came from
	!current state of the program is everything that we have used + k0

	restoreProgram:
		lw $sp, -1($sp)
		lw $a0, -2($sp)
		lw $a1, -3($sp)
		lw $at, -4($sp)
		lw $ra, -5($sp)
		lw $v0, -6($sp)
		!need to figure out what to do with the stack pointer
		di
		lw $k0, 0($sp)   !load k0 from the stack
		addi $sp, $sp, -7   ! pop 7 slots from the stack

	!remember to do a reti at the end of everything
	reti

hours:   .fill 0xF00002
minutes: .fill 0xF00001
seconds: .fill 0xF00000
initsp:  .fill 0xA00000
