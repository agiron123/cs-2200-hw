#include <stdlib.h>
#include <stdio.h>
#include "types.h"
#include "pagetable.h"
#include "global.h"
#include "process.h"

/*******************************************************************************
 * Finds a free physical frame. If none are available, uses a clock sweep
 * algorithm to find a used frame for eviction.
 *
 * @return The physical frame number of a free (or evictable) frame.
 */
pfn_t get_free_frame(void) {
   int i;

   for (i = 0; i < CPU_NUM_FRAMES; i++)
      if (rlt[i].pcb == NULL)
         return i;
	
   /* FIX ME : Problem 5 */
   /* IMPLEMENT A CLOCK SWEEP ALGORITHM HERE */
	   //if a page is used, set its used bit to 0
	   for(i = 0; i < CPU_NUM_FRAMES; i++)
	   {
				if(current_pagetable[i].valid == 0)
				{
						return current_pagetable[i].pfn;
				}
	   }

   //when we encounter a page that is not used, this is the page that we are going to evict
   //if we reach the end of memory, the search wraps 
   //else, if there are no invalid pages, perform a clock-sweep algorithm
   //to decide which page you should evict
		
	   for(int j = 0; j < CPU_NUM_FRAMES; j++)
	   {
				if(current_pagetable[j].used == 0)
					return current_pagetable[j].pfn;
				else
				{
					current_pagetable[j].used = 0;
				}
	   }

   /* If all else fails, return a random frame */
   return rand() % CPU_NUM_FRAMES;
}
