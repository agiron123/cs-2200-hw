/**
 * CS 2110 - Fall 2012 - Homework #12
 * Edited by: Brandon Whitehead, Andrew Wilder
 *
 * list.c: Complete the functions!
 */

#include <stdlib.h>
#include <stdio.h>
#include "list.h"

/* The node struct.  Has a prev pointer, next pointer, and data. */
/* DO NOT DEFINE ANYTHING OTHER THAN WHAT'S GIVEN OR YOU WILL GET A ZERO*/
/* Design consideration only this file should know about nodes */
/* Only this file should be manipulating nodes */
/* DO NOT TOUCH THIS DECLARATION DO NOT PUT IT IN OTHER FILES */
typedef struct lnode
{
  struct lnode* prev; /* Pointer to previous node */
  struct lnode* next; /* Pointer to next node */
  void* data; /* User data */
} node;


/* Do not create any global variables here. Your linked list library should obviously work for multiple linked lists */
/* This function is declared as static since you should only be calling this inside this file. */

/*Why is the create_node function prototype here?*/
static node* create_node(void* data);

/** create_node
  *
  * Helper function that creates a node by allocating memory for it on the heap.
  * Be sure to set its pointers to NULL.
  *
  * param data a void pointer to data the user wants to store in the list
  * return a node
  */
static node* create_node(void* data)
{
    node * temp;
	temp = malloc(sizeof(node));
	if(temp == NULL)
	{
		/*not sure what to return when null data is passed in*/
		return NULL;
	}
	
	temp-> next = NULL;
	temp-> prev = NULL;
	temp-> data = data;
    return temp;
}

/** create_list
  *
  * Creates a list by allocating memory for it on the heap.
  * Be sure to initialize size to zero and head/tail to NULL.
  *
  * return an empty linked list
  */
list* create_list(void)
{
    list * temp; 
    temp = malloc(sizeof(list));
    if(temp == NULL)
	{
		return NULL;
	}
	
	temp->head = NULL;
	temp->tail = NULL;
	temp->size = 0; 

    return temp;
}

/** push_front
  *
  * Adds the data to the front of the linked list
  *
  * param llist a pointer to the list.
  * param data pointer to data the user wants to store in the list.
  */
void push_front(list* llist, void* data)
{
    /*make a temporary node*/
    /*node* temp = create_node(data); */
    
    /*get the current head of the list*/
    node* head = llist->head;
    node* temp = create_node(data);
    
	if(llist->size == 0) /*check if the size of the list is 0 (list is empty) */
	{
		temp->next = NULL;
		temp->prev = NULL;
		llist->head = temp;
		llist->tail = temp;	
	}
    else if(head != NULL) /*check if the head is null*/
    {
		/*set head's prev pointer to the temp node*/
		head->prev = temp;
		/*set temp node's next pointer to head*/
		temp->next = head;
		/*make the temp pointer the head of the list*/
		llist->head = temp;
	}

	/*free(temp);*/
	llist->size++;
}

/** push_back
  *
  * Adds the data to the back/end of the linked list
  *
  * param llist a pointer to the list.
  * param data pointer to data the user wants to store in the list.
  */
void push_back(list* llist, void* data)
{
    /*make a temporary node*/
    node* temp = create_node(data);
    
    /*get the current tail of the list*/
    node* tail = llist->tail;
    
    /*check if the size of the list is 0*/
    if(llist->size == 0)
    {
		llist->head = temp; /*make the new node the head of the list*/
		llist->tail = temp; /*the new node is also the tail of the list*/
		temp->next = NULL;
		temp->prev = NULL;
	}
    else if(tail != NULL) /*check if the tail is null*/
    {
		/*printf("push_back: tail is not NULL\n");*/
		/*DEbug here*/
		/*set tail's next pointer to the temp node*/
		tail->next = temp;
		/*set temp node's prev pointer to tail*/
		temp->prev = tail;
		/*make the temp pointer the tail of the list*/
		llist->tail = temp;	
		temp->next = NULL;
	}
	/*may have to free temp*/
	llist->size++;
}

/** remove_front
  *
  * Removes the node at the front of the linked list
  *
  * warning Note the data the node is pointing to is also freed. If you have any pointers to this node's data it will be freed!
  *
  * param llist a pointer to the list.
  * param free_func pointer to a function that is responsible for freeing the node's data.
  * return -1 if the remove failed (which is only there are no elements) 0 if the remove succeeded.
  */
int remove_front(list* llist, list_op free_func)
{
    /*Get the head of the list*/
    node* head = llist->head;
    
	if(llist->size == 0) /*There is nothing to remove from the list*/
	{
		free_func(head->data); /*free the old tail's data*/
		free(head);
		return -1;
	}
	else if(llist->size > 1)
	{
		/*need to debug here:*/
		head->prev = NULL;
		llist->head = head->next;
		llist->head->prev = NULL;
		head->next = NULL;
	}
	else if(llist->size == 1)
	{
		llist->head = NULL;
		llist->tail = NULL;
	}

	free_func(head->data); /*free the old head's data*/
	free(head); /*free the old head*/
    llist->size--; /*decrement the size of the list*/

    return 0;
}

/** remove_back
  *
  * Removes the node at the back of the linked list
  *
  * warning Note the data the node is pointing to is also freed. If you have any pointers to this node's data it will be freed!
  *
  * param llist a pointer to the list.
  * param free_func pointer to a function that is responsible for freeing the node's data.
  * return -1 if the remove failed 0 if the remove succeeded.
  */
int remove_back(list* llist, list_op free_func)
{

    /*Get the tail of the list*/
    node* oldTail = llist->tail;
    
	
	if(llist->size == 0) /*There is nothing to remove from the list*/
	{
		
		free_func(oldTail->data); /*free the old tail's data*/
		free(oldTail);
		return -1;
	}
	else if(llist->size > 1)
	{
		oldTail->next = NULL;
		llist->tail = oldTail->prev;
		oldTail->prev = NULL;
		llist->tail->next = NULL;
	}
	else if(llist->size == 1)
	{
		llist->head = NULL;
		llist->tail = NULL;
		
		free(llist->head);
		free(llist->tail);
	}
	
	free_func(oldTail->data); /*free the old tail's data*/
	free(oldTail); /*free the old tail*/
    
    llist->size--;

    return 0;
}

/** copy_list
  *
  * Create a new list structure, new nodes, and new copies of the data by using
  * the copy function. Its implementation for any test structure must copy
  * EVERYTHING!
  *
  * param llist A pointer to the linked list to make a copy of
  * param copy_func A function pointer to a function that makes a copy of the
  *        data that's being used in this linked list, allocating space for
  *        every part of that data on the heap
  * return The linked list created by copying the old one
  */
list* copy_list(list* llist, list_cpy copy_func)
{
	list* copy = create_list();
	node* current = llist->head;
	node* temp;
	temp = malloc(sizeof(node));
	
	while(current != NULL)
	{
		temp->data = copy_func(current->data);
		/*need to figure out how to use the funciton pointer*/
		push_back(copy, temp->data);
		current = current->next;
	}
	
	free(temp);
	free(current);
	return copy;
}

/** front
  *
  * Gets the data at the front of the linked list
  * If the list is empty return NULL.
  *
  * param llist a pointer to the list
  * return The data at the first node in the linked list or NULL.
  */
void* front(list* llist)
{

    if(llist->head == NULL)
    {
		return NULL;
	}
    return llist->head->data;
}

/** back
  *
  * Gets the data at the "end" of the linked list
  * If the list is empty return NULL.
  *
  * param llist a pointer to the list
  * return The data at the last node in the linked list or NULL.
  */
void* back(list* llist)
{    
    if(llist->tail == NULL) 
    {
		return NULL;
	}
    return llist->tail->data;
}

/** size
  *
  * Gets the size of the linked list
  *
  * param llist a pointer to the list
  * return The size of the linked list
  */
int size(list* llist)
{
    return llist->size;
}

/** traverse
  *
  * Traverses the linked list calling a function on each node's data.
  *
  * param llist a pointer to a linked list.
  * param do_func a function that does something to each node's data.
  */
void traverse(list* llist, list_op do_func)
{
    /*node * current = malloc(sizeof(node*));*/
    node* current= llist->head;
    
    /*
    if(llist != NULL)
	{
		current = llist-> head;
	}
	* */
	
	while(current != NULL)
	{
		do_func(current->data);
		current = current-> next;
	}
}

/** remove_if
  *
  * Removes all nodes whose data when passed into the predicate function returns true
  *
  * param llist a pointer to the list
  * param pred_func a pointer to a function that when it returns true it will remove the element from the list and do nothing otherwise see list_pred.
  * param free_func a pointer to a function that is responsible for freeing the node's data
  * return the number of nodes that were removed.
  */
int remove_if(list* llist, list_pred pred_func, list_op free_func)
{
	node* current;
    node * temp; /*create a temp node for use later in the function*/
	int nodecount;
    
    if(llist->head == NULL)
    {
		return 0;
	}
    
    current = llist->head;
    nodecount = 0;
    
    
    while(current != NULL)
    {
		if(pred_func(current->data))
		{
			/*check if the current node is the head
			call remove head if so*/
			if(current == llist->head)
			{
				temp = current->next;
				remove_front(llist, free_func);
				nodecount++;
			}
			else if(current == llist->tail) /*the current node is at the tail of the list*/
			{
				temp = current->next;
				remove_back(llist, free_func);
				nodecount++;
			}
			else /*the current node is between two nodes*/
			{
				temp = current->next;
				/*might have to put the current back into the code later, dont know for now*/
				current->prev->next = current->next;
				current->next->prev = current->prev;
				current->next = NULL;
				current->prev = NULL;
				nodecount++;
				llist->size--;
			}
			
			current = temp; /*reassign the current node*/
		}
		else
		{
			current = current->next;
		}
		
	}

    return nodecount;
}

/** is_empty
  *
  * Checks to see if the list is empty.
  *
  * param llist a pointer to the list
  * return 1 if the list is indeed empty 0 otherwise.
  */
int is_empty(list* llist)
{
    /*note an empty list by the way we want you to implement it has a size of zero and head points to NULL.*/
    
    if(llist->head == NULL && llist->size == 0)
    {
		return 1;
	}
    
    return 0; /*the list is not empty*/
}

/** empty_list
  *
  * Empties the list after this is called the list should be empty.
  *
  * param llist a pointer to a linked list.
  * param free_func function used to free the node's data.
  *
  */
void empty_list(list* llist, list_op free_func)
{

    node* current = llist->tail;
    
    while(current != NULL)
    {
		current = current->prev;
		remove_back(llist, free_func);
	}
	
	free(current);
}
