/*
 * student.c
 * Multithreaded OS Simulation for CS 2200, Project 4
 *
 * This file contains the CPU scheduler for the simulation.  
 */

#include <assert.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>

#include "os-sim.h"

/*
 * current[] is an array of pointers to the currently running processes.
 * There is one array element corresponding to each CPU in the simulation.
 *
 * current[] should be updated by schedule() each time a process is scheduled
 * on a CPU.  Since the current[] array is accessed by multiple threads, you
 * will need to use a mutex to protect it.  current_mutex has been provided
 * for your use.
 */
static pcb_t **current, *queuehead, *queuetail;
static pthread_mutex_t current_mutex;
static pthread_mutex_t readyqueue_mutex;
static pthread_cond_t added_signal;

int staticScheduler;
int cpu_count;
int time_slice;

/*queue implementation*/
void enqueue(pcb_t *process)
{
	pthread_mutex_lock(&readyqueue_mutex);
	
	if(queuehead == NULL || queuetail == NULL)
	{
		queuehead = process;
		queuetail = process;
	}
	else
	{
		queuetail->next = process;
		queuetail = process;	
	}
	
	process->next = NULL;
	pthread_mutex_unlock(&readyqueue_mutex);
	
}

pcb_t * dequeue()
{
	pcb_t * oldHead;
	oldHead = queuehead;

	if(queuehead != NULL)
	{
	  oldHead = queuehead;
	  queuehead = queuehead->next;
	}
	else
	{
	  queuehead = NULL;
	  queuetail = NULL;
	}
	
	return oldHead;
}

/*dequeue the highest priority entry in the linked list */

pcb_t * dequeueHighestPriority()
{
  pcb_t * prev;
  pcb_t * curr;
  pcb_t * result;
  
  int highestPriority;
  int index;
  int removeIndex;
  int i;
  
  prev = queuehead;
  curr = prev->next;
  
  highestPriority = 0;
  index = 0;
  removeIndex = 0;
  
  /*traverse through the linked list and search for the highest priority */
  while(curr != NULL)
  {
    if(curr->static_priority > highestPriority)
    {
		highestPriority = curr->static_priority;
		removeIndex = index;
    }

    curr = curr->next;
    prev = prev->next;
    index++;
  }

  curr = queuehead;
  prev = curr->next;

  /*traverse to the index to remove */
  for(i = 0; i < removeIndex; i++)
  {
      curr = curr->next;
      prev = prev->next;
  }

  /* now we are at the node that needs to be removed, so remove it*/
  /*case for removing the head*/
  result = curr;
  if(prev == queuehead && queuetail != queuehead)
  {
	  prev->next = NULL;
	  queuehead = curr;
  }

  /*case for removing a node that is between two nodes*/
  if(curr != queuehead && curr != queuetail)
  {
	  prev->next = curr->next;
	  curr->next = NULL;
  }

  /*case for removing the tail node*/
  if(curr == queuetail)
  {
	  prev->next = NULL;
	  queuetail = prev;
  }

  /*remember to save what we are removing to result*/
  return result;
}


/*
 * schedule() is your CPU scheduler.  It should perform the following tasks:
 *
 *   1. Select and remove a runnable process from your ready queue which 
 *	you will have to implement with a linked list or something of the sort.
 *
 *   2. Set the process state to RUNNING
 *
 *   3. Call context_switch(), to tell the simulator which process to execute
 *      next on the CPU.  If no process is runnable, call context_switch()
 *      with a pointer to NULL to select the idle process.
 *	The current array (see above) is how you access the currently running process indexed by the cpu id. 
 *	See above for full description.
 *	context_switch() is prototyped in os-sim.h. Look there for more information 
 *	about it and its parameters.
 */
static void schedule(unsigned int cpu_id)
{
	pthread_mutex_lock(&readyqueue_mutex);
	pthread_mutex_lock(&current_mutex);

    /* extract first process from the ready queue*/
	current[cpu_id] = dequeue();
	if(current[cpu_id] != NULL)
	{
		current[cpu_id]->state = PROCESS_RUNNING;
	}
	context_switch(cpu_id, current[cpu_id], time_slice); /*call context switch and apss in the right time slice, should be -1 for everything except Round Robin*/
	pthread_mutex_unlock(&current_mutex);
	pthread_mutex_unlock(&readyqueue_mutex);
}


/*
 * idle() is your idle process.  It is called by the simulator when the idle
 * process is scheduled.
 *
 * This function should block until a process is added to your ready queue.
 * It should then call schedule() to select the process to run on the CPU.
 */
extern void idle(unsigned int cpu_id)
{
	pthread_mutex_lock(&readyqueue_mutex);
	while(queuehead == NULL)
	{
		pthread_cond_wait(&added_signal, &readyqueue_mutex);
	}
	pthread_mutex_unlock(&readyqueue_mutex);
	schedule(cpu_id);
}


/*
 * preempt() is the handler called by the simulator when a process is
 * preempted due to its timeslice expiring.
 *
 * This function should place the currently running process back in the
 * ready queue, and call schedule() to select a new runnable process.
 */
extern void preempt(unsigned int cpu_id)
{
    pthread_mutex_lock(&current_mutex);
    enqueue(current[cpu_id]);
    pthread_mutex_unlock(&current_mutex);
    schedule(cpu_id);
}


/*
 * yield() is the handler called by the simulator when a process yields the
 * CPU to perform an I/O request.
 *
 * It should mark the process as WAITING, then call schedule() to select
 * a new process for the CPU.
 */
extern void yield(unsigned int cpu_id)
{	
    pthread_mutex_lock(&current_mutex);
    current[cpu_id]->state = PROCESS_WAITING;
    pthread_mutex_unlock(&current_mutex);
    schedule(cpu_id);	
}


/*
 * terminate() is the handler called by the simulator when a process completes.
 * It should mark the process as terminated, then call schedule() to select
 * a new process for the CPU.
 */
extern void terminate(unsigned int cpu_id)
{
    pthread_mutex_lock(&current_mutex);
    current[cpu_id]->state = PROCESS_TERMINATED;
    pthread_mutex_unlock(&current_mutex);
    schedule(cpu_id);	
}


/*
 * wake_up() is the handler called by the simulator when a process's I/O
 * request completes.  It should perform the following tasks:
 *
 *   1. Mark the process as READY, and insert it into the ready queue.
 *
 *   2. If the scheduling algorithm is static priority, wake_up() may need
 *      to preempt the CPU with the lowest priority process to allow it to
 *      execute the process which just woke up.  However, if any CPU is
 *      currently running idle, or all of the CPUs are running processes
 *      with a higher priority than the one which just woke up, wake_up()
 *      should not preempt any CPUs.
 *	To preempt a process, use force_preempt(). Look in os-sim.h for 
 * 	its prototype and the parameters it takes in.
 */
extern void wake_up(pcb_t *process)
{
	int i;
	int lowestPriority;
	int shouldPreempt;
	int preemptCore;

	preemptCore = -5; /*sentinel value for preempt core */
	shouldPreempt = 1;
	lowestPriority = 10; /* 10 is the highest priority, we are going to compare values less than it */
	process->state = PROCESS_READY;
	pthread_mutex_lock(&current_mutex);

	enqueue(process);	
	if(staticScheduler == 1)
	{
		/*printf("Static Scheduler");*/
		/*check if any CPU is currently running idle or if all of the CPUs are running processes
		 * with a higher priority than the one which just woke up. If so, dont preempt any CPUs */
		 for(i = 0; i < cpu_count; i++)
		 {
			 /*printf("Wake up i: %d", i); */
			 /*preempt a lower priority process when a higher priority process needs a CPU.*/
			 if(current[i] == NULL) /*does what the comment above wants */
			 {
				 shouldPreempt = 0;
			 }
			 else if(current[i]->static_priority > process->static_priority)
		         {
			     shouldPreempt = 0;
	         	  }
			 
			 if(current[i]->static_priority < lowestPriority)
			 {
				 lowestPriority = current[i]->static_priority;
				 preemptCore = i;
				 shouldPreempt = 0;
			 }
		 }
		 
		 if(shouldPreempt == 1 && preemptCore != -5)
		 {
			 /*call force preempt for the core of the process that we should be preempting */
			 /*printf("\nStatic Scheduler Preempt core %d\n", preemptCore);*/
			 force_preempt(preemptCore);
		 }
	}
       
	pthread_mutex_unlock(&current_mutex);
	pthread_cond_signal(&added_signal); 	/*signal idle that something was added to the ready queue*/
}


/*
 * main() simply parses command line arguments, then calls start_simulator().
 * You will need to modify it to support the -r and -p command-line parameters.
 */
int main(int argc, char *argv[])
{
    int cpu_count;    
    /*
    if (argc != 2)
    {
        fprintf(stderr, "CS 2200 Project 4 -- Multithreaded OS Simulator\n"
            "Usage: ./os-sim <# CPUs> [ -r <time slice> | -p ]\n"
            "    Default : FIFO Scheduler\n"
            "         -r : Round-Robin Scheduler\n"
            "         -p : Static Priority Scheduler\n\n");
        return -1;
    }
    * 
    * */
    cpu_count = atoi(argv[1]);

    /* FIX ME - Add support for -r and -p parameters*/
	/*need to assign time slice to something if -r is passed in*/
	if(argc == 2)
	{
		time_slice = -1;
	}
	else if (argc == 3)
	{
		printf("argv[2][0]: %c", argv[2][1]);
		if(argv[2][0] == '-' && argv[2][1] == 'p') 
		{
			time_slice = -1;
			staticScheduler = 1;
		}
		
	}
	else if(argc == 4)
	{

		if(argv[2][0] == '-' && argv[2][1] == 'r') 
		{
			time_slice = atoi(argv[3]);
		}
	}

    /* Allocate the current[] array and its mutex */
    current = malloc(sizeof(pcb_t*) * cpu_count);
    
    assert(current != NULL);
    pthread_mutex_init(&current_mutex, NULL);
	pthread_mutex_init(&readyqueue_mutex, NULL);
	
	/*Check the command line arguments*/
	printf("Command line argument test \n");
	printf("Argc %d \n", argc);
	printf("CPU Count: %d \n", cpu_count);
	printf("Time Slice: %d \n", time_slice);
	printf("Static Scheduler: %d \n", staticScheduler);

    /* Start the simulator in the library */
    start_simulator(cpu_count);

    return 0;
}
