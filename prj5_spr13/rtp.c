#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <string.h>

#include "queue.h"
#include "network.h"
#include "rtp.h"

typedef struct _MESSAGE{
  char* buffer;
  int length;
} MESSAGE;

pthread_mutex_t bufferMutex;


/* ================================================================ */
/*                  H E L P E R    F U N C T I O N S                */
/* ================================================================ */

/*
 *  Returns a number computed based on the data in the buffer.
 * 
 *
 */
static int checksum(char *buffer, int length){
  /*  ----  FIXME  ----
   *
   *  The goal is to return a number that is determined by the contents
   *  of the buffer passed in as a parameter.  There a multitude of ways
   *  to implement this function.  For simplicity, simply sum the ascii
   *  values of all the characters in the buffer, and return the total.
   */ 
  /*Need to add support for pthreads and stuff */
  int checksum, i;
  checksum = 0;
  
  for(i = 0; i < length; i++)
  {
    checksum += buffer[i];
  }
  /*printf("Checksum: %d \n", checksum);*/
  return checksum;
}

/*
 *  Converts the given buffer into an array of PACKETs and returns
 *  the array.  The value of (*count) should be updated so that it 
 *  contains the length of the array created.
 */
 /* Length is how long each packet is */
 /* count is the number of packets that we have */
static PACKET* packetize(char *buffer, int length, int *count){

  /*  ----  FIXME  ----
   *
   *  The goal is to turn the buffer into an array of packets.
   *  You should allocate the space for an array of packets and
   *  return a pointer to the first element in that array.  The
   *  integer pointed to by 'count' should be updated to indicate
   *  the number of packets in the array.
   */ 
   
    PACKET* packets;
    int i, packetCount;
    packetCount = length / MAX_PAYLOAD_LENGTH;


    if(length % MAX_PAYLOAD_LENGTH)
    {
		packetCount += 1;
    }

    packets = (PACKET *)malloc(sizeof(PACKET)*(packetCount));

    for(i = 0; i < packetCount; i++)
    {
		if(i != packetCount -1)
		{
			packets[i].type = DATA;
			packets[i].payload_length = MAX_PAYLOAD_LENGTH;
		}
		else
		{
			packets[i].payload_length = length % MAX_PAYLOAD_LENGTH;
		}

		memcpy(packets[i].payload, &buffer[i* MAX_PAYLOAD_LENGTH], packets[i].payload_length);

		packets[i].checksum = checksum(packets[i].payload, packets[i].payload_length);
	}
	
	packets[packetCount -1].type = LAST_DATA;
	*count = packetCount;

    return packets;
}

/* ================================================================ */
/*                      R T P       T H R E A D S                   */
/* ================================================================ */

	    /*  ----  FIXME  ----
	     *
	     * 2. send an ACK or a NACK, whichever is appropriate

	     * 4. if the payload matches, add the payload to the buffer
	     *    as done below
	     */
           /*  ----  FIXME  ----
            * 
            *  What if the packet received is not a data packet?
            *  If it is a NACK or an ACK, the sending thread should
            *  be notified so that it can finish sending the message.
            *   
            *  1. add the necessary fields to the CONNECTION data structure
            *     in rtp.h so that the sending thread has a way to determine
            *     whether a NACK or an ACK was received
            *  2. signal the sending thread that an ACK or a NACK has been
            *     received.
            * 
            * 
            */

static void *rtp_recv_thread(void *void_ptr){

    RTP_CONNECTION *connection = (RTP_CONNECTION*)void_ptr;

    do
    {
		PACKET temp;
        MESSAGE *message;
        char *buffer = NULL;
        int buffer_length = 0;
        PACKET packet;
		message = 0; /*initialize message to some value */

        /* 
        * put messages in buffer until the last packet is received 
        */  
        do
        {
		/*printf("In the recv thread method's do loop\n"); */
		if (net_recv_packet(connection->net_connection_handle, &packet) != 1 || packet.type == TERM)
		{
			  /* remote side has disconnected */
			  connection->alive = 0;
			  pthread_cond_signal(&(connection->recv_cond));
			  pthread_cond_signal(&(connection->send_cond));
			  break;
		}

	    /*Compare the checksum of the packet that they sent in to the checksum that we calculte*/
    
	    /* 1. check to make sure payload of packet is correct (above) */
	    if(packet.type == DATA && checksum(packet.payload, packet.payload_length) == packet.checksum)
	    {			
			  /*printf("Recv thread: Checksum matched expected value\n"); */
			
			  buffer_length += packet.payload_length; /*make sure to also do this for last data */
			
			  buffer = realloc(buffer, buffer_length); /*make room for the buffer */
			  memcpy(&buffer[buffer_length-packet.payload_length], packet.payload, packet.payload_length);
		
			  pthread_mutex_lock(&connection->status_mutex);
			  connection->status = ACKCOND;
			  
			  temp.type = ACK;
			  net_send_packet(connection->net_connection_handle, &(temp));

			  pthread_mutex_unlock(&connection->status_mutex);
  			  pthread_cond_signal(&(connection->packet_ready_cond));
			  /*printf("Recv thread: sent an ACK over the network\n");*/
	    }
	    else if(packet.type == DATA && checksum(packet.payload, packet.payload_length) != packet.checksum)
	    {
			  /*send a NACK because they do not match */
			  /*printf("Recv thread: sending a NACK over the network\n");*/
			  pthread_mutex_lock(&connection->status_mutex);
			  connection->status = NACKCOND;
			  temp.type = NACK;
			  net_send_packet(connection->net_connection_handle, &(temp));
			  pthread_mutex_unlock(&connection->status_mutex);
			  pthread_cond_signal(&(connection->packet_ready_cond));
			  /*printf("Recv thread: sent a NACK over the network\n");*/
	    }
	    
	    /*just trying to put this if statment here */
		if(packet.type == LAST_DATA && checksum(packet.payload, packet.payload_length) == packet.checksum) /*need to make sure that we are dealing with the last data right */
		{
			  /*printf("Recv thread: in the last data condition\n"); */
			  buffer_length += packet.payload_length;
			  buffer = realloc(buffer, buffer_length); /*make room for the buffer */
			  memcpy(&buffer[buffer_length-packet.payload_length], packet.payload, packet.payload_length);
			  pthread_mutex_lock(&connection->status_mutex);
			  connection->status = ACKCOND;
			  temp.type = ACK;
			  net_send_packet(connection->net_connection_handle, &(temp));
			  pthread_mutex_unlock(&connection->status_mutex);
			  pthread_cond_signal(&(connection->packet_ready_cond));
		}
		else if(packet.type == LAST_DATA && checksum(packet.payload, packet.payload_length) != packet.checksum)
		{
			  pthread_mutex_lock(&connection->status_mutex);
			  connection->status = ACKCOND;
			  temp.type = ACK;
			  net_send_packet(connection->net_connection_handle, &(temp));
			  pthread_mutex_unlock(&connection->status_mutex);
			  pthread_cond_signal(&(connection->packet_ready_cond));			
		}
	    
        } while (packet.type != LAST_DATA);   /*Might want to change to something which checks if we have gotten the last packet */

		if(packet.type == LAST_DATA && checksum(packet.payload, packet.payload_length) == packet.checksum) /*need to make sure that we are dealing with the last data right */
		{
			  /*printf("Recv thread: in the last data condition\n"); */
			  buffer_length += packet.payload_length;
			  buffer = realloc(buffer, buffer_length); /*make room for the buffer */
			  memcpy(&buffer[buffer_length-packet.payload_length], packet.payload, packet.payload_length);
			  pthread_mutex_lock(&connection->status_mutex);
			  connection->status = ACKCOND;
			  temp.type = ACK;
			  net_send_packet(connection->net_connection_handle, &(temp));
			  pthread_mutex_unlock(&connection->status_mutex);
			  pthread_cond_signal(&(connection->packet_ready_cond));
		}
		else if(packet.type == LAST_DATA && checksum(packet.payload, packet.payload_length) != packet.checksum)
		{
			  pthread_mutex_lock(&connection->status_mutex);
			  connection->status = ACKCOND;
			  temp.type = ACK;
			  net_send_packet(connection->net_connection_handle, &(temp));
			  pthread_mutex_unlock(&connection->status_mutex);
			  pthread_cond_signal(&(connection->packet_ready_cond));			
		}
		
        if (connection->alive == 1)
        {
			/* --- FIX ME -----
			 *
			 * Add message to the received queue here.
			 */
			/*printf("Recv thread: Adding received message to the queue\n");*/
			
			message = malloc(sizeof(MESSAGE));
			message->buffer = buffer;
			message->length = buffer_length; /* used to be = */
			
			pthread_mutex_lock(&(connection->recv_mutex));
			queue_add(&(connection->recv_queue), message);
			pthread_mutex_unlock(&(connection->recv_mutex));
			/*printf("Recv thread: Finished adding received message to the queue \n");*/
			pthread_cond_signal(&connection->recv_cond);
        } else free(buffer);
    
    } while (connection->alive == 1);

    return NULL;
}


static void *rtp_send_thread(void *void_ptr){

    RTP_CONNECTION *connection = (RTP_CONNECTION*)void_ptr;
    MESSAGE *message;
    int array_length, i;
    PACKET *packet_array;
    
    do {
        
        /*printf("Inside the rtp_send_thread method \n");*/
        
        /* extract the next message from the send queue */
        /* -------------------------------------------- */
        pthread_mutex_lock(&(connection->send_mutex));
        while (queue_size(&(connection->send_queue)) == 0 &&
               connection->alive == 1)
            pthread_cond_wait(&(connection->send_cond), &(connection->send_mutex));
        
        if (connection->alive == 0)
            break;
        
        message = queue_extract(&(connection->send_queue));
        
        pthread_mutex_unlock(&(connection->send_mutex));
        
        /* packetize the message and send it */
        /* --------------------------------- */
        packet_array = packetize(message->buffer, message->length, &array_length);
        /*printf("RTP_SEND_THREAD: Got the packet array \n");*/
        
        for (i=0; i < array_length; i++) {
            
            /* Start sending the packetized messages */
            /* ------------------------------------- */
            if (net_send_packet(connection->net_connection_handle, &(packet_array[i])) < 0){
                /* remote side has disconnected */
                connection->alive = 0;
                break;
            }
            
            /*  ----FIX ME ---- 
             * 
             *  1. wait for the recv thread to notify you of when a NACK or
             *     an ACK has been received
             *  2. check the data structure for this connection to determine
             *     if an ACK or NACK was received.  (You'll have to add the
             *     necessary fields yourself)
             *  3. If it was an ACK, continue sending the packets.
             *  4. If it was a NACK, resend the last packet
             */
			 
			 /*printf("RTP_SEND_THREAD: Connection status: %d \n", connection->status);*/
	 
			 while(connection->status != NACKCOND && connection->status!= ACKCOND)
			 {
				/* printf("RTP_SEND_THREAD: Waiting for an ack or a nack \n"); */
				 pthread_cond_wait(&(connection->packet_ready_cond), &(connection->status_mutex));
				 /*pthread_cond_wait(&(connection->recv_cond), &(connection->status_mutex)); */
			 }
			 
			/* pthread_mutex_lock(&connection->status_mutex); */

			 if(connection->status == NACKCOND)
			 {
				 /*printf("RTP_SEND_THREAD: Got a NACK, resending the last packet \n");*/
				 i--;
				 /*pthread_cond_signal(&connection->packet_ready_cond);*/
			 } /*keep sending packets if we get an ACK */
			 else if(connection->status == ACKCOND)
			 {
				/*send a packet here */
				/*printf("RTP_SEND_THREAD: GOT an ACK, keep sending packets \n");*/
				/*pthread_cond_signal(&connection->packet_ready_cond);*/ /*not sure if we need this*/
			 }
        }

        /*printf("RTP_SEND_THREAD: Freeing things\n");*/
        free(packet_array);
        free(message->buffer);
        free(message);
        
    } while(connection->alive == 1);
    return NULL;
}

RTP_CONNECTION *rtp_init_connection(int net_connection_handle){
  RTP_CONNECTION *rtp_connection = malloc(sizeof(RTP_CONNECTION));
 
  if (rtp_connection == NULL){
    fprintf(stderr,"Out of memory!\n");
    exit (EXIT_FAILURE);
  }

  rtp_connection->net_connection_handle = net_connection_handle;

  queue_init(&(rtp_connection->recv_queue));
  queue_init(&(rtp_connection->send_queue));

  pthread_mutex_init(&(rtp_connection->ack_mutex), NULL);
  pthread_mutex_init(&(rtp_connection->recv_mutex),NULL);
  pthread_mutex_init(&(rtp_connection->send_mutex),NULL);
  pthread_cond_init(&(rtp_connection->ack_cond), NULL);
  pthread_cond_init(&(rtp_connection->recv_cond),NULL);
  pthread_cond_init(&(rtp_connection->send_cond),NULL);

  rtp_connection->alive = 1;

  pthread_create(&(rtp_connection->recv_thread), NULL, rtp_recv_thread,
		 (void*)rtp_connection);
  pthread_create(&(rtp_connection->send_thread), NULL, rtp_send_thread,
		 (void*)rtp_connection);

  return rtp_connection;
}

/* ================================================================ */
/*                           R T P    A P I                         */
/* ================================================================ */

RTP_CONNECTION *rtp_connect(char *host, int port){
  
  int net_connection_handle;

  if ((net_connection_handle = net_connect(host,port)) < 1)
    return NULL;

  return (rtp_init_connection(net_connection_handle));
}

int rtp_disconnect(RTP_CONNECTION *connection){

  MESSAGE *message;
  PACKET term;

  term.type = TERM;
  term.payload_length = term.checksum = 0;
  net_send_packet(connection->net_connection_handle, &term);
  connection->alive = 0;
  
  net_disconnect(connection->net_connection_handle);
  pthread_cond_signal(&(connection->send_cond));
  pthread_cond_signal(&(connection->recv_cond));
  pthread_join(connection->send_thread,NULL);
  pthread_join(connection->recv_thread,NULL);
  
  /* emtpy recv queue and free allocated memory */
  while ((message = queue_extract(&(connection->recv_queue))) != NULL){
    free(message->buffer);
    free(message);
  }

  /* emtpy send queue and free allocated memory */
  while ((message = queue_extract(&(connection->send_queue))) != NULL){
    free(message);
  }

  free(connection);

  return 1;

}

int rtp_recv_message(RTP_CONNECTION *connection, char **buffer, int *length){

  MESSAGE *message;

  if (connection->alive == 0) 
    return -1;
  /* lock */
  printf("Locking recv_mutex1\n");
  pthread_mutex_lock(&(connection->recv_mutex));
  printf("Locked recv_mutex1\n");
  while (queue_size(&(connection->recv_queue)) == 0 &&
	 connection->alive == 1)
    pthread_cond_wait(&(connection->recv_cond), &(connection->recv_mutex));
  
  if (connection->alive == 0) {
    printf("Unlocking recv_mutex2\n");
    pthread_mutex_unlock(&(connection->recv_mutex));
    printf("Unlocked recv_mutex2\n");
    return -1;
  }

  /* extract */
  message = queue_extract(&(connection->recv_queue));
  *buffer = message->buffer;
  *length = message->length;
  free(message);

  /* unlock */
  printf("Unlocking recv_mutex1\n");
  pthread_mutex_unlock(&(connection->recv_mutex));
  printf("Unlocked recv_mutex1\n");

  return *length;
}

int rtp_send_message(RTP_CONNECTION *connection, char *buffer, int length){

  MESSAGE *message;

  if (connection->alive == 0) 
    return -1;

  message = malloc(sizeof(MESSAGE));
  if (message == NULL)
    return -1;
  message->buffer = malloc(length);
  message->length = length;

  if (message->buffer == NULL){
    free(message);
    return -1;
  }

  memcpy(message->buffer,buffer,length);

  /* lock */
  printf("Locking send_mutex2\n");
  pthread_mutex_lock(&(connection->send_mutex));
  printf("Locked send_mutex2\n");
  
  /* add */
  queue_add(&(connection->send_queue),message);

  /* unlock */
  printf("Unlocking send_mutex2\n");
  pthread_mutex_unlock(&(connection->send_mutex));
  printf("Unlocked send_mutex2\n");
  pthread_cond_signal(&(connection->send_cond));
  return 1;

}
