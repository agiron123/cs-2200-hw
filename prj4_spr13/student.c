/*
 * student.c
 * Multithreaded OS Simulation for CS 2200, Project 4
 *
 * This file contains the CPU scheduler for the simulation.  
 */

#include <assert.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include "os-sim.h"
#include "list.h"

typedef struct lnode
{
  struct lnode* prev; /* Pointer to previous node */
  struct lnode* next; /* Pointer to next node */
  void* data; /* User data */
} node;

/*
 * current[] is an array of pointers to the currently running processes.
 * There is one array element corresponding to each CPU in the simulation.
 *
 * current[] should be updated by schedule() each time a process is scheduled
 * on a CPU.  Since the current[] array is accessed by multiple threads, you
 * will need to use a mutex to protect it.  current_mutex has been provided
 * for your use.
 */
static pcb_t **current;

static pthread_mutex_t current_mutex;
static pthread_mutex_t readyqueue_mutex; /*mutex for the ready queue*/
static pthread_mutex_t currptr_mutex; /*mutex for the current pointer*/

static pthread_cond_t idle_condition;
/*static pthread_cond_t preempt_condition; */
/* static pthread_cond_t schedule_condition; */
/* static pthread_cond_t terminate_condition; */
/* static pthread_cond_t wake_up_condition; */
/* static pthread_cond_t yield_condition; */


int staticScheduler;
int cpu_count;
int time_slice;

static list *readyqueue;
node* curr;
/*
 * schedule() is your CPU scheduler.  It should perform the following tasks:
 *
 *   1. Select and remove a runnable process from your ready queue which 
 *	you will have to implement with a linked list or something of the sort.
 *
 *   2. Set the process state to RUNNING
 *
 *   3. Call context_switch(), to tell the simulator which process to execute
 *      next on the CPU.  If no process is runnable, call context_switch()
 *      with a pointer to NULL to select the idle process.
 *	The current array (see above) is how you access the currently running process indexed by the cpu id. 
 *	See above for full description.
 *	context_switch() is prototyped in os-sim.h. Look there for more information 
 *	about it and its parameters.
 */
static void schedule(unsigned int cpu_id)
{
	
	printf("In Schedule %d", cpu_id);
	pthread_mutex_lock(&readyqueue_mutex);
	pthread_mutex_lock(&currptr_mutex);
	
	pcb_t* process;
	
	process = front(readyqueue);
	
	if(process != NULL)
	{
		remove_front(readyqueue->head);
		pthread_mutex_lock(&current_mutex);
		current[cpu_id] = head;
		current[cpu_id]->state = PROCESS_RUNNING;
	}
	
	
	printf("Schedule: about to get to the while loop");

    /* FIX ME */
    /* Need to get a runnable process from the ready queue (which I implement) */
	/*Make sure to lock the CPU and then unlock it at the end*/
    curr = readyqueue->head;
    if(curr == NULL)
    {
		printf("Schedule: Head is null");
	}
    int foundRunnable = 0;
    /*node* curr = front(readyqueue);*/
    while(curr->data != NULL)
    {
		printf("Schedule: In the while loop");

		 pcb_t* data = (pcb_t*)curr->data;
         if(((int)(pcb_t*)data->state) == PROCESS_READY) 	/*find runnable process*/
         {
			printf("Schedule: Found runnable process");
            data->state = PROCESS_RUNNING; 	/*set process state to running*/
            foundRunnable = 1;
         }
         context_switch(cpu_id, data, -1); 			/*tell the simulator which process to execute next*/
         curr = curr->next; /*move to the next process*/
         /*if no process runnable, call context switch with a pointer to NULL to select the idle process*/
    }
    
    if(foundRunnable == 0)
    {
		printf("Schedule: Did not find runnable process");
		context_switch(cpu_id, NULL, -1);
	}
	
	pthread_mutex_unlock(&readyqueue_mutex);
}

/*
 * idle() is your idle process.  It is called by the simulator when the idle
 * process is scheduled.
 *
 * This function should block until a process is added to your ready queue.
 * It should then call schedule() to select the process to run on the CPU.
 */
extern void idle(unsigned int cpu_id)
{
	
	printf("In idle %d", cpu_id);
    /* FIX ME */
    /*idle conditon is going to be called when we should call this method*/
    
    
		pthread_cond_wait(&idle_condition, &current_mutex);
	
		schedule(cpu_id);

    /*
     * REMOVE THE LINE BELOW AFTER IMPLEMENTING IDLE()
     *
     * idle() must block when the ready queue is empty, or else the CPU threads
     * will spin in a loop.  Until a ready queue is implemented, we'll put the
     * thread to sleep to keep it from consuming 100% of the CPU time.  Once
     * you implement a proper idle() function using a condition variable,
     * remove the call to mt_safe_usleep() below.
     */
      /*  mt_safe_usleep(1000000); */
}

/*
 * preempt() is the handler called by the simulator when a process is
 * preempted due to its timeslice expiring.
 *
 * This function should place the currently running process back in the
 * ready queue, and call schedule() to select a new runnable process.
 */
extern void preempt(unsigned int cpu_id)
{
	
	printf("In preempt %d", cpu_id);
    /* FIX ME */
    pthread_mutex_lock(&readyqueue_mutex);
    push_front(readyqueue, current[cpu_id]);      /*place currently running process back into the ready queue*/
    pthread_mutex_unlock(&readyqueue_mutex);
    schedule(cpu_id); /*call schedule to select a new runnable process*/
}


/*
 * yield() is the handler called by the simulator when a process yields the
 * CPU to perform an I/O request.
 *
 * It should mark the process as WAITING, then call schedule() to select
 * a new process for the CPU.
 */
extern void yield(unsigned int cpu_id)
{
	printf("In yield %d", cpu_id);
    pthread_mutex_lock(&current_mutex);
    current[cpu_id]->state = PROCESS_WAITING;
    pthread_mutex_unlock(&current_mutex);
    schedule(cpu_id);
}

/*
 * terminate() is the handler called by the simulator when a process completes.
 * It should mark the process as terminated, then call schedule() to select
 * a new process for the CPU.
 */
extern void terminate(unsigned int cpu_id)
{
	printf("In Terminate %d", cpu_id);
	/*TODO: Add threading support */
	pthread_mutex_lock(&current_mutex);
    current[cpu_id]->state = PROCESS_TERMINATED;
    pthread_mutex_unlock(&current_mutex);
    /*schedule(cpu_id);*/
}


/*
 * wake_up() is the handler called by the simulator when a process's I/O
 * request completes.  It should perform the following tasks:
 *
 *   1. Mark the process as READY, and insert it into the ready queue.
 *
 *   2. If the scheduling algorithm is static priority, wake_up() may need
 *      to preempt the CPU with the lowest priority process to allow it to
 *      execute the process which just woke up.  However, if any CPU is
 *      currently running idle, or all of the CPUs are running processes
 *      with a higher priority than the one which just woke up, wake_up()
 *      should not preempt any CPUs.
 *	To preempt a process, use force_preempt(). Look in os-sim.h for 
 * 	its prototype and the parameters it takes in.
 * 
 * extern void force_preempt(unsigned int cpu_id);
 */
extern void wake_up(pcb_t *process)
{
	printf("In Wakeup ");

    /* FIX ME */
	pthread_mutex_lock(&readyqueue_mutex);
	pthread_mutex_lock(&current_mutex);
	
	/*use a condition here*/
    /*Initialize the ready queue here*/
    /*
    if(NULL == readyqueue)
    {
		readyqueue = create_list();
	}
	*/
	/*mark the process as ready*/
	process->state = PROCESS_READY;
	/*put the process into the ready queue*/
	push_front(readyqueue, process);
		
	/*if scheduling algo is static priority, may need to preempt the CPU with the lowest
	 * priority process to allow it to execute the process it just woke up*/
	 
	 /*
	  if(staticScheduler == 1)
	  {
		 * Make sure that no CPU is currently idle or all of the CPUs are running processes with a higher
		  * priority than the one which just woke up. If so, dont preempt any CPUs 
		  
		 *int lowestPriority = cpu_count + 1; *set lowest priority to a high number
		 *int lowestPrioritycore = 0;
		 *lowestPrioritycore += 0; *core with the lowest priority process on it
		 *int should_preempt = 1; if we break from the loop, we shouldnt preempt process- logic taken care of there
		 *int i;
		 lower the number the higher the priority
		 for(i= 0; i < cpu_count; i++)
		 {
			if(process->static_priority > current[i]->static_priority && current[i]->state != 0)
			{
				if(current[i]->static_priority < lowestPriority)
				{
					lowestPriority = current[i]->static_priority;
					lowestPrioritycore = i;
				}
			}
			else
			{
				should_preempt = 0; we should not preempt the process
				break; exit the for loop
			}
		 }
		 
		 if(should_preempt)
		 {
				force_preempt(lowestPrioritycore);
		 }
	 }
	 */
	pthread_mutex_unlock(&current_mutex);
	pthread_mutex_unlock(&readyqueue_mutex);

}


/*
 * main() simply parses command line arguments, then calls start_simulator().
 * You will need to modify it to support the -r and -p command-line parameters.
 */
int main(int argc, char *argv[])
{
	
    /* Parse command-line arguments */
    if (argc < 2)
    {
        fprintf(stderr, "CS 2200 Project 4 -- Multithreaded OS Simulator\n"
            "Usage: ./os-sim <# CPUs> [ -r <time slice> | -p ]\n"
            "    Default : FIFO Scheduler\n"
            "         -r : Round-Robin Scheduler\n"
            "         -p : Static Priority Scheduler\n\n");
        return -1;
    }
    cpu_count = atoi(argv[1]);


    /* FIX ME - Add support for -r and -p parameters (done)*/

	/*need to assign time slice to something if -r is passed in*/
	if(argc > 2)
	{
		time_slice = atoi(argv[3]);
		/* time_slice += 0; */
	}
	else if(argc == 4)
	{
		time_slice = atoi(argv[3]);
		staticScheduler = 1;
	}

    /* Allocate the current[] array and its mutex */
    current = malloc(sizeof(pcb_t*) * cpu_count);
    readyqueue = create_list();
    curr = readyqueue->head;
    
    assert(current != NULL);
    pthread_mutex_init(&current_mutex, NULL);

	assert(readyqueue != NULL);
	pthread_mutex_init(&readyqueue_mutex, NULL);

	pthread_mutex_init(&currptr_mutex, NULL);

    /* Start the simulator in the library */
    start_simulator(cpu_count); 
    
    printf("CPU Count %d \n", cpu_count);
    printf("Time slice %d \n", time_slice);

    return 0;
}
